package me.xxmatthdxx.control.region;

import me.xxmatthdxx.control.game.Arena;
import me.xxmatthdxx.control.game.GameManager;
import me.xxmatthdxx.control.utils.ShapeUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthew on 2015-07-24.
 */
public class RegionManager {

    private static RegionManager instance = new RegionManager();

    private List<Region> regions = new ArrayList<Region>();

    public static RegionManager getInstance(){
        return instance;
    }

    public void setup(){
        for(Arena arena : GameManager.getInstance().getArenas()){
            for(Region region : arena.getRegions()){
                regions.add(region);
                ShapeUtil.generate(region.getCenter(), Material.WOOL);
            }
        }
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    public boolean isInRegion(Player pl){
        for(Region region : regions){
            if(region.getCenter().distanceSquared(pl.getLocation()) <= 5){
                return true;
            }
        }
        return false;
    }

    public Region getRegion(Player pl){
        for(Region region : regions){
            if(region.getCenter().distanceSquared(pl.getLocation()) <= 5){
                return region;
            }
        }
        return null;
    }
}
