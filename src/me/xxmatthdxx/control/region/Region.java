package me.xxmatthdxx.control.region;

import me.xxmatthdxx.control.game.Arena;
import me.xxmatthdxx.control.teams.Team;
import org.bukkit.Location;

import java.util.List;

/**
 * Created by Matthew on 2015-07-24.
 */
public class Region {

    private Location center;
    private int bravoControl;
    private int alphaControl;
    Arena arena;
    private boolean controled;
    private Team controler;

    /**
     * Blue is positive
     * Red negative
     */

    private int radius = 5;

    public Region(Arena arena, Location center) {
        this.arena = arena;
        this.center = center;
        controler = null;
        controled = false;
        alphaControl = 0;
        bravoControl = 0;
    }

    public Location getCenter() {
        return center;
    }

    public void setCenter(Location center) {
        this.center = center;
    }

    public void incrementBravoControl(int increment) {
        this.bravoControl = (bravoControl + increment);
    }

    public void incrementAlphaControl(int increment){
        this.alphaControl = (alphaControl + increment);
    }

    public int getBravoControl(){
        return bravoControl;
    }

    public int getAlphaControl(){
        return alphaControl;
    }

    public Arena getArena() {
        return arena;
    }

    public void setArena(Arena arena) {
        this.arena = arena;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public boolean isControled() {
        return controled;
    }

    public void setControled(boolean controled) {
        this.controled = controled;
    }

    public Team getControler() {
        return controler;
    }

    public void setControler(Team controler) {
        this.controler = controler;
    }
}
