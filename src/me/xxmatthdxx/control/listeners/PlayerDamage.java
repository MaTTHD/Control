package me.xxmatthdxx.control.listeners;

import me.xxmatthdxx.control.game.GameManager;
import me.xxmatthdxx.control.teams.Team;
import me.xxmatthdxx.control.teams.TeamManager;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * Created by Matthew on 2015-07-29.
 */
public class PlayerDamage implements Listener {

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e) {
        if (!(e.getEntity() instanceof Player) && !(e.getDamager() instanceof Player)) {
            return;
        }

        Player damager = (Player) e.getDamager();
        Player killed = (Player) e.getEntity();

        if ((killed.getHealth() - e.getDamage()) <= 0) {
            killed.setHealth(killed.getMaxHealth());
            if (TeamManager.getInstance().getTeam(killed).getName().equalsIgnoreCase("bravo")) {
                killed.teleport(GameManager.getInstance().getCurrentArena().getBravo());
            } else {
                killed.teleport(GameManager.getInstance().getCurrentArena().getAlpha());
            }
            Team team = TeamManager.getInstance().getTeam(damager);
            team.addPoints(team.getModifier() + 100);
            damager.sendMessage(org.bukkit.ChatColor.GREEN + "Received " + (team.getModifier() + 100) + " points (100 for kill " + team.getModifier() + " for the team modifier.) for neutralizing a enemy");
        }
        return;
    }
}
