package me.xxmatthdxx.control.listeners;

import me.xxmatthdxx.control.teams.TeamManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Matthew on 2015-07-29.
 */
public class PlayerLeave implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        Player pl = e.getPlayer();

        if(TeamManager.getInstance().getTeam(pl) != null){
            TeamManager.getInstance().getTeam(pl).getMembers().remove(pl);
        }
    }
}
