package me.xxmatthdxx.control.listeners;

import me.xxmatthdxx.control.game.GameManager;
import me.xxmatthdxx.control.teams.Team;
import me.xxmatthdxx.control.teams.TeamManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.List;

/**
 * Created by Matthew on 2015-07-24.
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        //TODO check if they have a party via nebular

        Player pl = e.getPlayer();
        List<Team> teams = TeamManager.getInstance().getTeams();

        if(teams.get(0).getMembers().size() >= teams.get(1).getMembers().size()){
            TeamManager.getInstance().addTeam(pl, teams.get(1));
        }
        else {
            TeamManager.getInstance().addTeam(pl, teams.get(0));
        }
    }
}
