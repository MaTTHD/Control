package me.xxmatthdxx.control.teams;

import me.xxmatthdxx.control.Control;
import me.xxmatthdxx.control.game.GameState;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthew on 2015-07-26.
 */
public class TeamManager {

    List<Team> teams = new ArrayList<Team>();

    public List<Team> getTeams() {
        return teams;
    }

    private static TeamManager instance = new TeamManager();

    public static TeamManager getInstance(){
        return instance;
    }


    public Team getTeam(String name){
        for(Team team : teams){
            if (team.getName().equals(name)){
                return team;
            }
        }
        return null;
    }

    public Team getTeam(Player pl){
        for(Team team : teams){
            if (team.getMembers().contains(pl.getUniqueId())){
                return team;
            }
        }
        return null;
    }

    public void addTeam(Player pl, Team team){
        if(Control.getPlugin().getState() != GameState.WAITING){
            pl.sendMessage(ChatColor.RED + "You cannot change your team at this time!");
            return;
        }

        if(getTeam(pl) != null){
           pl.sendMessage(ChatColor.RED + "You are already in a team!");
            return;
        }

        if((team.getMembers().size() +1)  >= team.getMaxSize()){
            pl.sendMessage(ChatColor.RED + "This team is full!");
            return;
        }

        team.getMembers().add(pl.getUniqueId());
        pl.sendMessage(ChatColor.GREEN + "Joined team " + team.getName());
    }
}
