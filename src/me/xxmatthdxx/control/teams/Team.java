package me.xxmatthdxx.control.teams;

import me.xxmatthdxx.control.Control;
import me.xxmatthdxx.control.game.GameManager;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Matthew on 2015-07-24.
 */
public class Team {

    private Control plugin = Control.getPlugin();

    private String name;
    private List<UUID> members;
    private int maxSize = plugin.getConfig().getInt("team_size");
    private String prefix;
    private int points;
    private int modifier;

    public Team(String name) {
        this.name = name;
        this.members = new ArrayList<UUID>();
        this.points = 0;
        this.modifier = 0;
    }

    public int getPoints() {
        return points;
    }

    public void addPoints(int points) {
        this.points = (this.points + points);

        if(this.points >= 2000){
            GameManager.getInstance().stop(this);
        }
        else {

        }
    }

    public int getModifier() {
        return modifier;
    }

    public void setModifier(int modifier) {
        this.modifier = modifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UUID> getMembers() {
        return members;
    }

    public void setMembers(List<UUID> members) {
        this.members = members;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
