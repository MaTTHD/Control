package me.xxmatthdxx.control.game;

import me.xxmatthdxx.control.Control;
import me.xxmatthdxx.control.teams.Team;
import me.xxmatthdxx.control.teams.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Matthew on 2015-07-24.
 */
public class GameManager {

    private static GameManager instance = new GameManager();

    private List<Arena> arenas = new ArrayList<Arena>();

    public Arena currentArena;
    public Arena lastPlayed;


    public static GameManager getInstance() {
        return instance;
    }

    public List<Arena> getArenas() {
        return arenas;
    }

    public Arena getArena(String name) {
        for (Arena arena : arenas) {
            if (arena.getName().equalsIgnoreCase(name)) {
                return arena;
            }
        }
        return null;
    }

    public Arena getCurrentArena() {
        return currentArena;
    }

    public Arena getLastPlayed() {
        return lastPlayed;
    }

    public void init() {
        Team bravo = new Team("Bravo");
        Team alpha = new Team("Alpha");

        TeamManager.getInstance().getTeams().add(bravo);
        TeamManager.getInstance().getTeams().add(alpha);

        for (String name : Control.getPlugin().getArenas().getKeys(false)) {
            Arena arena = new Arena(name);
            System.out.println(arena.getName());
            arenas.add(arena);
        }
    }

    public void start() {
        boolean map = chooseMap();
        if (map) {
            Control.getPlugin().setState(GameState.INGAME);
            Control.getPlugin().setTicks(8 * 60);
            List<Team> teams = TeamManager.getInstance().getTeams();
            for (Team team : teams) {
                for (UUID uuid : team.getMembers()) {
                    Player pl = Bukkit.getPlayer(uuid);
                    pl.getInventory().setItem(0, new ItemStack(Material.IRON_SWORD));
                    pl.getInventory().setHeldItemSlot(0);
                    if (TeamManager.getInstance().getTeam(pl).getName().equalsIgnoreCase("bravo")) {
                        pl.teleport(currentArena.getBravo());
                    } else {
                        pl.teleport(currentArena.getAlpha());
                    }
                }
            }
        } else {
            forceStop(ChatColor.RED + "There was a error loading the map, sending you back to hub!");
        }
    }

    public void stop(Team team) {
        Bukkit.broadcastMessage(ChatColor.RED + "Team " + team.getName() + " has won this game!");

        new BukkitRunnable() {
            public void run() {
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    pl.kickPlayer(ChatColor.RED + "Game over");
                }
                Bukkit.shutdown();
            }
        }.runTaskLater(Control.getPlugin(), 20L * 4);
    }

    public void forceStop(String reason) {
        Bukkit.broadcastMessage(reason);
        for (Player pl : Bukkit.getOnlinePlayers()) {
            pl.kickPlayer("Game restarting");
        }

        Bukkit.shutdown();
    }

    public boolean chooseMap() {
        Random ran = new Random();
        Arena arenaToChoose;

        if (lastPlayed == null) {
            arenaToChoose = arenas.get(ran.nextInt(arenas.size() + 1));
            currentArena = arenaToChoose;
            return true;
        }

        if (arenas.get(ran.nextInt(arenas.size() + 1)) == lastPlayed) {
            chooseMap();
        } else {
            arenaToChoose = arenas.get(ran.nextInt(arenas.size() + 1));
            currentArena = arenaToChoose;
            return true;
        }
        return false;
    }
}
