package me.xxmatthdxx.control.game;

/**
 * Created by Matthew on 2015-07-24.
 */
public enum GameState {

    WAITING, INGAME, DISABLED;
}
