package me.xxmatthdxx.control.game;

import me.xxmatthdxx.control.Control;
import me.xxmatthdxx.control.region.Region;
import me.xxmatthdxx.control.region.RegionManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Matthew on 2015-07-24.
 */
public class Arena {

    private String name;
    private Location bravo, alpha;
    private Location p1, p2;

    private List<Region> regions = new ArrayList<Region>();

    private Control plugin = Control.getPlugin();

    public Arena(String name) {
        this.name = name;

        String[] bravoArray = plugin.getArenas().getString(name + ".bravo").split(",");
        double x = Double.valueOf(bravoArray[0]);
        double y = Double.valueOf(bravoArray[1]);
        double z = Double.valueOf(bravoArray[2]);
        World world = Bukkit.getWorld(bravoArray[3]);


        String[] alphaArray = plugin.getArenas().getString(name + ".bravo").split(",");
        double x1 = Double.valueOf(alphaArray[0]);
        double y1 = Double.valueOf(alphaArray[1]);
        double z1 = Double.valueOf(alphaArray[2]);
        World world1 = Bukkit.getWorld(alphaArray[3]);

        bravo = new Location(world, x,y,z);
        alpha = new Location(world1, x1, y1, z1);

        for(String region : plugin.getArenas().getStringList(name + ".regions")){
            String[] regionArray = region.split(",");
            double x2 = Double.valueOf(regionArray[0]);
            double y2 = Double.valueOf(regionArray[1]);
            double z2 = Double.valueOf(regionArray[2]);
            World world2 = Bukkit.getWorld(regionArray[3]);

            Region regions = new Region(this, new Location(world2, x2, y2,z2));
            this.regions.add(regions);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getBravo() {
        return bravo;
    }

    public void setBravo(Location bravo) {
        this.bravo = bravo;
    }

    public Location getAlpha() {
        return alpha;
    }

    public void setAlpha(Location alpha) {
        this.alpha = alpha;
    }

    public List<Region> getRegions(){
        return regions;
    }

    public void addRegion(Location location){
        Region region = new Region(this, location);
        this.regions.add(region);
        RegionManager.getInstance().getRegions().add(region);
    }
}
