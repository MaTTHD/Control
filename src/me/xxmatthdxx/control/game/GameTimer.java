package me.xxmatthdxx.control.game;

import me.xxmatthdxx.control.Control;
import me.xxmatthdxx.control.message.MessageManager;
import me.xxmatthdxx.control.teams.Team;
import me.xxmatthdxx.control.teams.TeamManager;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

/**
 * Created by Matthew on 2015-07-24.
 */
public class GameTimer extends BukkitRunnable {

    private Control plugin = Control.getPlugin();
    private MessageManager msg = MessageManager.getInstance();

    public void run() {
        if (plugin.getTicks() > 0) {
            plugin.setTicks(plugin.getTicks() - 1);
        }

        if (plugin.getState() == GameState.WAITING) {
            if (plugin.getTicks() != 0) {
                if (plugin.getTicks() % 15 == 0) {
                    Bukkit.broadcastMessage(msg.getMsg("game_start_message").replace("{TIME}", Integer.toString(plugin.getTicks())));
                }
                if (plugin.getTicks() < 5) {
                    Bukkit.broadcastMessage(msg.getMsg("game_start_message").replace("{TIME}", Integer.toString(plugin.getTicks())));
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        pl.playNote(pl.getLocation(), Instrument.PIANO, Note.flat(1, Note.Tone.A));
                    }
                }
            } else {
                if (Bukkit.getOnlinePlayers().size() < plugin.getConfig().getInt("min_players")) {
                    plugin.setTicks(plugin.getConfig().getInt("lobby_time"));
                    Bukkit.broadcastMessage(msg.getMsg("not_enough_players").replace("{PLAYERS}", Integer.toString(plugin.getConfig().getInt("min_players"))));
                } else {
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        pl.playSound(pl.getLocation(), Sound.ENDERDRAGON_GROWL, 1, 1);
                    }
                    GameManager.getInstance().start();
                }
            }
        } else if (plugin.getState() == GameState.INGAME) {
            if (plugin.getTicks() != 0) {
                if (plugin.getTicks() % 15 == 0) {
                    Bukkit.broadcastMessage(msg.getMsg("game_end_message").replace("{TIME}", plugin.secondsToString(plugin.getTicks())));
                }
                if (plugin.getTicks() < 5) {
                    Bukkit.broadcastMessage(msg.getMsg("game_end_message").replace("{TIME}", plugin.secondsToString(plugin.getTicks())));
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        pl.playNote(pl.getLocation(), Instrument.PIANO, Note.flat(1, Note.Tone.A));
                    }
                }
            } else {
                List<Team> teams = TeamManager.getInstance().getTeams();

                if (teams.get(0).getPoints() > teams.get(1).getPoints()) {
                    Team winner = teams.get(0);
                    GameManager.getInstance().stop(winner);
                    Bukkit.broadcastMessage(ChatColor.GREEN + "Time limit reached! " + winner.getName() + " wins the game!");
                } else {
                    Team winner = teams.get(1);
                    GameManager.getInstance().stop(winner);
                    Bukkit.broadcastMessage(ChatColor.GREEN + "Time limit reached! " + winner.getName() + " wins the game!");
                }
            }
        }
    }
}
