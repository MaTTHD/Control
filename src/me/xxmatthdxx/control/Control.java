package me.xxmatthdxx.control;

import me.xxmatthdxx.control.cmds.CommandManager;
import me.xxmatthdxx.control.game.GameManager;
import me.xxmatthdxx.control.game.GameState;
import me.xxmatthdxx.control.game.GameTimer;
import me.xxmatthdxx.control.listeners.PlayerDamage;
import me.xxmatthdxx.control.listeners.PlayerJoin;
import me.xxmatthdxx.control.listeners.PlayerLeave;
import me.xxmatthdxx.control.listeners.PlayerMove;
import me.xxmatthdxx.control.region.Region;
import me.xxmatthdxx.control.region.RegionManager;
import me.xxmatthdxx.control.teams.Team;
import me.xxmatthdxx.control.teams.TeamManager;
import me.xxmatthdxx.control.utils.ShapeUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Matthew on 2015-07-24.
 */
public class Control extends JavaPlugin {

    private static Control plugin;

    private int ticks;
    private GameState state;

    private File arenaFile;
    private FileConfiguration arenas;

    public void onEnable() {
        plugin = this;

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new PlayerMove(), this);
        pm.registerEvents(new PlayerLeave(), this);
        pm.registerEvents(new PlayerDamage(), this);

        CommandManager cm = new CommandManager();

        getCommand("arena").setExecutor(cm);

        arenaFile = new File(getDataFolder(), "arenas.yml");

        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
            getConfig().options().copyDefaults(true);
            saveDefaultConfig();
        } else {
            saveConfig();

        }

        if (!arenaFile.exists()) {
            try {
                arenaFile.createNewFile();
                arenas = YamlConfiguration.loadConfiguration(arenaFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        arenas = YamlConfiguration.loadConfiguration(arenaFile);
        saveArenas();


        if (getConfig().getBoolean("is_offline")) {
            state = GameState.DISABLED;
        } else {

            GameManager.getInstance().init();
            RegionManager.getInstance().setup();

            GameTimer timer = new GameTimer();
            timer.runTaskTimer(this, 0L, 20L);

            ticks = getConfig().getInt("lobby_time");
            state = GameState.WAITING;
        }
        new BukkitRunnable() {
            public void run() {
                if(state != GameState.INGAME){
                    return;
                }
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    Team team = TeamManager.getInstance().getTeam(pl);

                    System.out.println(team.getName());

                    if (team.getName().equalsIgnoreCase("bravo")) {
                        if (RegionManager.getInstance().isInRegion(pl)) {
                            Region region = RegionManager.getInstance().getRegion(pl);
                            if (region.getBravoControl() < 100) {
                                if(region.getAlphaControl() > 0){
                                    region.incrementAlphaControl(-25);
                                    pl.sendMessage(ChatColor.GREEN + "Alpha control: " + region.getAlphaControl() + " / 100");
                                }
                                else {
                                    pl.sendMessage(ChatColor.GREEN + "Bravo control: " + region.getBravoControl() + " / 100");
                                    region.incrementBravoControl(25);
                                }
                            } else if (region.getBravoControl() >= 100) {
                                if (region.isControled()) {
                                    if (region.getControler() != null && region.getControler() != TeamManager.getInstance().getTeam(pl)) {
                                        Bukkit.broadcastMessage(TeamManager.getInstance().getTeam(pl).getName() +
                                                " has taken control of the region at co-ordinates: (" +
                                                region.getCenter().getBlockX() + "," + region.getCenter().getBlockY() + "," +
                                                region.getCenter().getBlockY() + ") from team: " + region.getControler().getName());
                                        region.setControler(TeamManager.getInstance().getTeam(pl));
                                        region.setControled(true);
                                        int currentMultiplier = TeamManager.getInstance().getTeam("alpha").getModifier();
                                        int newMultiplier = (currentMultiplier - 25);
                                        TeamManager.getInstance().getTeam("alpha").setModifier(newMultiplier);
                                    }
                                } else {
                                    region.setControler(TeamManager.getInstance().getTeam(pl));
                                    region.setControled(true);
                                    Bukkit.broadcastMessage(TeamManager.getInstance().getTeam(pl).getName() +
                                            " has taken control of the region at co-ordinates: (" +
                                            region.getCenter().getBlockX() + "," + region.getCenter().getBlockY() + "," +
                                            region.getCenter().getBlockY() + ")");
                                }
                                ShapeUtil.generate(region.getCenter(), Material.REDSTONE_BLOCK);
                                team.setModifier((team.getModifier() + 25));
                            }
                        }
                    } else if (team.getName().equalsIgnoreCase("alpha")) {
                        if (RegionManager.getInstance().isInRegion(pl)) {
                            System.out.println("In region - alpha");
                            Region region = RegionManager.getInstance().getRegion(pl);

                            if (region == null) {
                                System.out.println("REGION NULL");
                            }

                            if (region.getCenter() == null) {
                                System.out.println("LOCATION IS NULL!");
                            }

                            if (region.getAlphaControl() < 100) {
                                if(region.getBravoControl() > 0){
                                    region.incrementBravoControl(-25);
                                    pl.sendMessage(ChatColor.GREEN + "Bravo control: " + region.getBravoControl() + " / 100");
                                }
                                else {
                                    pl.sendMessage(ChatColor.GREEN + "Alpha control: " + region.getAlphaControl() + " / 100");
                                    region.incrementAlphaControl(25);
                                }
                            } else if (region.getAlphaControl() >= 100) {
                                if (region.isControled()) {
                                    if (region.getControler() != null && region.getControler() != TeamManager.getInstance().getTeam(pl)) {
                                        Bukkit.broadcastMessage(TeamManager.getInstance().getTeam(pl).getName() +
                                                " has taken control of the region at co-ordinates: (" +
                                                region.getCenter().getBlockX() + "," + region.getCenter().getBlockY() + "," +
                                                region.getCenter().getBlockY() + ") from team: " + region.getControler().getName());
                                        region.setControler(TeamManager.getInstance().getTeam(pl));
                                        region.setControled(true);
                                        int currentMultiplier = TeamManager.getInstance().getTeam("bravo").getModifier();
                                        int newMultiplier = (currentMultiplier - 25);
                                        TeamManager.getInstance().getTeam("bravo").setModifier(newMultiplier);
                                    }
                                } else {
                                    region.setControler(TeamManager.getInstance().getTeam(pl));
                                    region.setControled(true);
                                    Bukkit.broadcastMessage(TeamManager.getInstance().getTeam(pl).getName() +
                                            " has taken control of the region at co-ordinates: (" +
                                            region.getCenter().getBlockX() + "," + region.getCenter().getBlockY() + "," +
                                            region.getCenter().getBlockY() + ")");
                                }
                                ShapeUtil.generate(region.getCenter(), Material.LAPIS_BLOCK);
                                team.setModifier((team.getModifier() + 25));
                            }
                        }
                    }
                }
            }
        }.runTaskTimer(this, 0L, 20 * 5L);
    }

    public void onDisable() {

    }

    public static Control getPlugin() {
        return plugin;
    }

    public int getTicks() {
        return ticks;
    }

    public void setTicks(int ticks) {
        this.ticks = ticks;
    }

    public GameState getState() {
        return state;
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public String secondsToString(int ptime) {
        return String.format("%02d:%02d", ptime / 60, ptime % 60);
    }

    public void saveArenas() {
        try {
            arenas.save(arenaFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public FileConfiguration getArenas() {
        return arenas;
    }
}

