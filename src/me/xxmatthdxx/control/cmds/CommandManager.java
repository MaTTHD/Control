package me.xxmatthdxx.control.cmds;

import me.xxmatthdxx.control.Control;
import me.xxmatthdxx.control.game.Arena;
import me.xxmatthdxx.control.game.GameManager;
import me.xxmatthdxx.control.utils.ShapeUtil;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Matthew on 2015-07-26.
 */
public class CommandManager implements CommandExecutor {

    private Control plugin = Control.getPlugin();

    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {

        if (cmd.getName().equalsIgnoreCase("arena")) {
            if (!(sender instanceof Player)) {
                return false;
            }

            Player pl = (Player) sender;

            if (!pl.hasPermission("arena.admin")) {
                return false;
            }

            if (args.length == 2) {
                if (args[0].equalsIgnoreCase("create")) {
                    String name = args[1];

                    if (GameManager.getInstance().getArena(name) != null) {
                        pl.sendMessage(ChatColor.RED + "Arena with this name already exists.");
                        return false;
                    } else {

                        double x = pl.getLocation().getX();
                        double y = pl.getLocation().getY();
                        double z = pl.getLocation().getZ();
                        World world = pl.getLocation().getWorld();


                        plugin.getArenas().set(name + ".bravo", x + "," + y + "," + z + "," + world.getName().trim());
                        plugin.getArenas().set(name + ".alpha", x + "," + y + "," + z + "," + world.getName().trim());
                        List<String> reg = plugin.getArenas().getStringList(name + ".regions");
                        reg.add(x + "," + y + "," + z + "," + world.getName().trim());
                        plugin.getArenas().set(name + ".regions", reg);
                        plugin.saveArenas();

                        Arena arena = new Arena(name);
                        ShapeUtil.generate(pl.getLocation(), Material.WOOL);
                        arena.addRegion(pl.getLocation());
                        pl.sendMessage(ChatColor.GREEN + "Created arena " + name);
                    }
                }
            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("set")) {
                    String name = args[1];
                    if (args[2].equalsIgnoreCase("bravo")) {
                        if (GameManager.getInstance().getArena(name) == null) {
                            pl.sendMessage(ChatColor.RED + "No arena with this name exists.");
                            return false;
                        } else {

                            double x = pl.getLocation().getX();
                            double y = pl.getLocation().getY();
                            double z = pl.getLocation().getZ();
                            World world = pl.getLocation().getWorld();

                            plugin.getArenas().set(name + ".bravo", x + "," + y + "," + z + "," + world.getName().trim());
                            plugin.saveArenas();
                            pl.sendMessage(ChatColor.GREEN + "Set the bravos spawn to your current location.");

                        }
                    } else if (args[2].equalsIgnoreCase("alpha")) {
                        if (GameManager.getInstance().getArena(name) == null) {
                            pl.sendMessage(ChatColor.RED + "no arena with this name exists.");
                            return false;
                        } else {
                            double x = pl.getLocation().getX();
                            double y = pl.getLocation().getY();
                            double z = pl.getLocation().getZ();
                            World world = pl.getLocation().getWorld();

                            plugin.getArenas().set(name + ".alpha", x + "," + y + "," + z + "," + world.getName().trim());
                            plugin.saveArenas();
                            pl.sendMessage(ChatColor.GREEN + "Set the alpha spawn to your current location.");

                        }
                    }
                } else if (args[0].equalsIgnoreCase("add")) {
                    //add name point
                    String name = args[1];
                    if (args[2].equalsIgnoreCase("point")) {
                        if (GameManager.getInstance().getArena(name) == null) {
                            pl.sendMessage(ChatColor.RED + "No arena with this name exists.");
                            return false;
                        } else {
                            double x = pl.getLocation().getX();
                            double y = pl.getLocation().getY();
                            double z = pl.getLocation().getZ();
                            World world = pl.getLocation().getWorld();

                            List<String> reg = plugin.getArenas().getStringList(name + ".regions");
                            reg.add(x + "," + y + "," + z + "," + world.getName().trim());
                            plugin.getArenas().set(name + ".regions", reg);
                            plugin.saveArenas();

                            ShapeUtil.generate(pl.getLocation(), Material.WOOL);
                            GameManager.getInstance().getArena(name).addRegion(pl.getLocation());
                            pl.sendMessage(ChatColor.GREEN + "Added another region to arena: " + name);
                        }
                    }
                }
            }
        }
        return false;
    }
}
