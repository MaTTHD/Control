package me.xxmatthdxx.control.message;

import me.xxmatthdxx.control.Control;
import net.md_5.bungee.api.ChatColor;

/**
 * Created by Matthew on 2015-07-24.
 */
public class MessageManager {

    private static MessageManager instance = new MessageManager();

    public static MessageManager getInstance() {
        return instance;
    }

    public String getMsg(String path){
        return ChatColor.translateAlternateColorCodes('&', Control.getPlugin().getConfig().getString(path));
    }
}
